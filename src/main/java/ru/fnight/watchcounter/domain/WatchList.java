package ru.fnight.watchcounter.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WatchList {
    private Long id;
    @JsonProperty("user_id")
    private Long userId;
    private String name;
    private String description;
    @JsonProperty("canbedeleted")
    private boolean canBeDeleted;

    public WatchList() {
    }

    public WatchList(Long userId, String name, String description, boolean canBeDeleted) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.canBeDeleted = canBeDeleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCanBeDeleted() {
        return canBeDeleted;
    }

    public void setCanBeDeleted(boolean canBeDeleted) {
        this.canBeDeleted = canBeDeleted;
    }
}
