package ru.fnight.watchcounter.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Media {
    public static String MOVIE_TYPE = "movie";
    public static String SERIAL_TYPE = "tv";

    protected Long id;
    @JsonProperty("ext_id")
    protected Long extId;
    @JsonProperty("list_id")
    protected Long listId;
    @JsonProperty("media_type")
    private String mediaType;
    protected Integer rating;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExtId() {
        return extId;
    }

    public void setExtId(Long extId) {
        this.extId = extId;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public Integer getRating() { return rating; }

    public void setRating(Integer rating) { this.rating = rating; }

}
