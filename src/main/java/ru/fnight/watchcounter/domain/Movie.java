package ru.fnight.watchcounter.domain;

public class Movie extends Media {
    private boolean watched;

    public boolean isWatched() {
        return watched;
    }

    public void setWatched(boolean watched) {
        this.watched = watched;
    }
}
