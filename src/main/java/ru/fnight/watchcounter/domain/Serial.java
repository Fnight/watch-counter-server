package ru.fnight.watchcounter.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Serial extends Media {
    @JsonProperty("watched_series")
    private Integer watchedSeries;

    public int getWatchedSeries() {
        return watchedSeries;
    }

    public void setWatchedSeries(Integer watchedSeries) {
        this.watchedSeries = watchedSeries;
    }
}
