package ru.fnight.watchcounter.service;

import java.util.List;
import ru.fnight.watchcounter.domain.Media;
import ru.fnight.watchcounter.domain.WatchList;

public interface ListService {
    WatchList createList(String name, String description, Long userId, boolean canBeDeleted);

    WatchList createList(WatchList list);

    void createDefaultLists(Long userId);

    WatchList findListById(Long id);

    List<WatchList> findListsByUserId(Long id);

    WatchList updateList(WatchList watchList);

    void deleteList(Long id, boolean force);
}
