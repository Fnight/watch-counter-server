package ru.fnight.watchcounter.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.fnight.watchcounter.domain.Media;
import ru.fnight.watchcounter.domain.Movie;
import ru.fnight.watchcounter.domain.Serial;
import ru.fnight.watchcounter.service.ListService;
import ru.fnight.watchcounter.service.MediaService;
import ru.fnight.watchcounter.service.UserService;

@Service
public class MediaServiceImpl implements MediaService {

    @Autowired
    DataSource dataSource;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ListService listService;

    @Autowired
    UserService userService;

    private String getTableNameByMediaType(String mediaType) {
        if (mediaType.equals(Media.MOVIE_TYPE)) {
            return "movie";
        } else if (mediaType.equals(Media.SERIAL_TYPE)) {
            return "serial";
        } else {
            throw new IllegalArgumentException("wrong media type");
        }
    }

    private void validationMedia (Media media) {
        boolean rating = media.getRating() < 0 || media.getRating() > 10;
        if (rating) {
            throw new IllegalArgumentException("Rating is out of range");
        }
    }

    @Override
    public Media addNewMedia(String mediaType, Long extId, Long listId) {
        String tableName = getTableNameByMediaType(mediaType);
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).usingColumns("list_id", "ext_id")
                .withTableName(tableName).usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("list_id", listId);
        parameters.put("ext_id", extId);

        Long id = simpleJdbcInsert.executeAndReturnKey(parameters).longValue();

        return findMediaById(mediaType, id);
    }

    @Override
    public Media findMediaById(String mediaType, Long id) {
        if (mediaType.equals(Media.MOVIE_TYPE)) {
            return findMovieById(id);
        } else {
            return findSerialById(id);
        }
    }

    @Override
    public Serial findSerialById(Long id) {
        List<Serial> serials = jdbcTemplate.query("select id, list_id, ext_id, watched_series, rating from serial where id=?",
                new BeanPropertyRowMapper<>(Serial.class), id);
        if (serials.isEmpty()) {
            throw new IllegalArgumentException("serial with id = '" + id + "' not found");
        } else {
            return serials.get(0);
        }
    }

    @Override
    public Movie findMovieById(Long id) {
        List<Movie> movies = jdbcTemplate.query("select id, list_id, ext_id, watched, rating from movie where id=?",
                new BeanPropertyRowMapper<>(Movie.class), id);
        if (movies.isEmpty()) {
            throw new IllegalArgumentException("movie with id = '" + id + "' not found");
        } else {
            return movies.get(0);
        }
    }

    private List<Movie> findMoviesByListId(Long listId) {
        return jdbcTemplate.query("select id, list_id, ext_id, watched, rating from movie where list_id=?",
                new BeanPropertyRowMapper<>(Movie.class), listId);
    }

    private List<Serial> findSerialsByListId(Long listId) {
        return jdbcTemplate.query("select id, list_id, ext_id, watched_series, rating from serial where list_id=?",
                new BeanPropertyRowMapper<>(Serial.class), listId);
    }

    @Override
    public List<Media> findMediasByListId(Long listId) {
        List<Media> totalMediaList = new ArrayList<>();
        List<Movie> movies = findMoviesByListId(listId);
        movies.forEach(movie -> movie.setMediaType(Media.MOVIE_TYPE));
        totalMediaList.addAll(movies);
        List<Serial> serials = findSerialsByListId(listId);
        serials.forEach(serial -> serial.setMediaType(Movie.SERIAL_TYPE));
        totalMediaList.addAll(serials);
        return totalMediaList;
    }

    @Override
    public void deleteMediaById(Long id, String mediaType) {
        Media media = findMediaById(mediaType, id);
        if (!userService.checkRulesForMedia(media)) {
            throw new SecurityException("cannot delete media with id '" + id + "' by not owner");
        }
        String tableName = getTableNameByMediaType(mediaType);
        jdbcTemplate.update("delete from " + tableName + " where id = ?", id);
    }

    @Override
    public Movie updateMovie(Movie movie) {
        if (userService.checkRulesForMedia(movie)) {
            validationMedia(movie);
            jdbcTemplate.update("update movie set (list_id, watched, rating) = (?, ?, ?) where id=?", movie.getListId(),
                    movie.isWatched(), movie.getRating(), movie.getId());
            return findMovieById(movie.getId());
        } else {
            throw new SecurityException("cannot change movie another user");
        }
    }

    @Override
    public Serial updateSerial(Serial serial) {
        if (userService.checkRulesForMedia(serial)) {
            validationMedia(serial);
            jdbcTemplate.update("update serial set (list_id, watched_series, rating) = (?, ?, ?) where id=?", serial.getListId(),
                    serial.getWatchedSeries(), serial.getRating(), serial.getId());
            return findSerialById(serial.getId());
        } else {
            throw new SecurityException("cannot change serial another user");
        }
    }

    @Override
    public List<Long> findAllUniqueMovieExtId() {
        return jdbcTemplate.queryForList("select distinct ext_id from movie", Long.class);
    }

    @Override
    public List<Long> findAllUniqueSerialExtId() {
        return jdbcTemplate.queryForList("select distinct ext_id from serial", Long.class);
    }
}
