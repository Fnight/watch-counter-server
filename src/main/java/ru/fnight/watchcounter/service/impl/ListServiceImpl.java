package ru.fnight.watchcounter.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.fnight.watchcounter.domain.Media;
import ru.fnight.watchcounter.domain.Movie;
import ru.fnight.watchcounter.domain.Serial;
import ru.fnight.watchcounter.domain.WatchList;
import ru.fnight.watchcounter.service.ListService;
import ru.fnight.watchcounter.service.MediaService;
import ru.fnight.watchcounter.service.UserService;

@Service
public class ListServiceImpl implements ListService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    UserService userService;

    @Autowired
    MediaService mediaService;

    @Autowired
    DataSource dataSource;

    @Override
    public WatchList createList(String name, String description, Long userId, boolean canBeDeleted) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("list").usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("user_id", userId);
        parameters.put("name", name);
        parameters.put("description", description);
        parameters.put("canbedeleted", canBeDeleted);
        Long id = simpleJdbcInsert.executeAndReturnKey(parameters).longValue();
        return findListById(id);
    }

    @Override
    public WatchList createList(WatchList list) {
        return createList(list.getName(), list.getDescription(), list.getUserId(), list.isCanBeDeleted());
    }

    @Override
    public void createDefaultLists(Long userId) {
        WatchList planned = new WatchList(userId, "Запланировано",
                "Список запланированного для просмотра", false);
        WatchList watching = new WatchList(userId, "Смотрю",
                "Активный список просмотра", false);
        WatchList deferred = new WatchList(userId, "Отложено",
                "Список отложенных медиа", false);
        WatchList watched = new WatchList(userId, "Просмотрено",
                "Список просмотренного", false);
        WatchList abandoned = new WatchList(userId, "Брошено",
                "Список не интресного или заброшенного", false);
        WatchList[] watchLists = new WatchList[] {planned, watching, watched, deferred, abandoned};
        for (WatchList list : watchLists) {
            createList(list);
        }
    }

    @Override
    public WatchList findListById(Long id) {
        List<WatchList> watchLists = jdbcTemplate.query("select id, name, user_id, description, canbedeleted  from list where id=?",
                new BeanPropertyRowMapper<>(WatchList.class), id);
        if (watchLists.isEmpty()) {
            throw new RuntimeException("list with id =  '" + id + "' not found");
        } else {
            return watchLists.get(0);
        }
    }

    @Override
    public List<WatchList> findListsByUserId(Long id) {
        return jdbcTemplate.query("select id, name, user_id, description, canbedeleted from list where user_id=?",
                new BeanPropertyRowMapper<>(WatchList.class), id);
    }

    @Override
    public WatchList updateList(WatchList watchList) {
        if (userService.checkRulesForList(watchList)) {
            jdbcTemplate.update("update list set (name, description) = (?, ?) where id = ?",
                    watchList.getName(), watchList.getDescription(), watchList.getId());
            return findListById(watchList.getId());
        } else {
            throw new SecurityException("cannot change list another user");
        }
    }

    @Override
    public void deleteList(Long id, boolean force) {
        WatchList list = findListById(id);
        if (!list.isCanBeDeleted() && !force) {
            throw new SecurityException("this list cannot be deleted");
        }
        if (!userService.checkRulesForList(list)) {
            throw new SecurityException("cannot delete list another user");
        }
        List<Media> medias = mediaService.findMediasByListId(list.getId());
        medias.forEach(media -> {
            if (media instanceof Movie) {
                mediaService.deleteMediaById(media.getId(), Media.MOVIE_TYPE);
            } else if (media instanceof Serial) {
                mediaService.deleteMediaById(media.getId(), Media.SERIAL_TYPE);
            }
        });
        jdbcTemplate.update("delete from list where id=?", list.getId());
    }
}
