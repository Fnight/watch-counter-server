package ru.fnight.watchcounter.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;
import ru.fnight.watchcounter.service.TestService;


@Service
public class TestServiceImpl implements TestService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public String getting() {
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("select * from test");
        sqlRowSet.next();
        String string = sqlRowSet.getString(1);
        return "\n" + string;
    }
}
