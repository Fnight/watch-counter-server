package ru.fnight.watchcounter.service.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import ru.fnight.watchcounter.domain.Media;
import ru.fnight.watchcounter.domain.User;
import ru.fnight.watchcounter.domain.WatchList;
import ru.fnight.watchcounter.service.ListService;
import ru.fnight.watchcounter.service.UserService;
import ru.fnight.watchcounter.web.rest.dto.UserRegisterDto;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    DataSource dataSource;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ListService listService;

    private static Logger logger = LogManager.getLogger();

    @Override
    public User register(UserRegisterDto userRegisterDto, String ip) {
        Long lastRegistration = lastRegistration(ip).getTime();
        if (!userRegisterDto.isValid()) {
            throw new IllegalArgumentException("wrong user data");
        }
        if ((System.currentTimeMillis() - lastRegistration) < 10*60*1000) {
            throw new SecurityException("last retry can be only after 10 minutes");
        }
        Timestamp now = new Timestamp(System.currentTimeMillis());
        try {
            User user = createNewUser(userRegisterDto.getName(), userRegisterDto.getPassword(),
                    userRegisterDto.getEmail());
            insertNewRegistration(now, ip);
            return user;
        } catch (DuplicateKeyException ex) {
            throw new IllegalArgumentException("Такой пользователь уже существует");
        }
    }

    private void insertNewRegistration(Timestamp time, String ip) {
        jdbcTemplate.update("update registrations set timestamp=?, ip=? where ip=?",
                time, ip, ip);
        jdbcTemplate.update("insert into registrations (timestamp, ip)" +
                " select ?, ?" +
                " where not exists (select 1 from registrations where ip=?)", time, ip, ip);
    }

    @Override
    public Timestamp lastRegistration(String ip) {
        List<Timestamp> times = jdbcTemplate.queryForList("select timestamp from registrations where ip=?",
                new String[]{ip}, Timestamp.class);
        Timestamp last = times.stream().max(Timestamp::compareTo).orElse(new Timestamp(0));
        logger.info(last);
        return last;
    }

    @Override
    public User getUserByName(String name) {
        List<User> users = jdbcTemplate.query("select id, username, email, role from users where username=?",
                new BeanPropertyRowMapper<>(User.class), name);
        if (users.isEmpty()) {
            throw new RuntimeException("user '" + name + "' not found");
        } else {
            return users.get(0);
        }
    }

    @Override
    public User getCurrentUser() {
        UserDetails userDetails =
                (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return getUserByName(userDetails.getUsername());
    }

    @Override
    public User getUserById(Long id){
        List<User> users = jdbcTemplate.query("select id, username, email, role from users where id=?",
                new BeanPropertyRowMapper<>(User.class), id);
        if (users.isEmpty()) {
            throw new RuntimeException("user with id'" + id + "' not found");
        } else {
            return users.get(0);
        }
    }

    @Override
    public User createNewUser(String name, String rawPassword, String email) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).usingColumns("username", "password", "email")
                .withTableName("users").usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("username", name);
        parameters.put("password", BCrypt.hashpw(rawPassword, BCrypt.gensalt()));
        parameters.put("email", email);

        Long id = simpleJdbcInsert.executeAndReturnKey(parameters).longValue();
        User newUser = getUserById(id);
        listService.createDefaultLists(newUser.getId());
        return newUser;
    }

    @Override
    public boolean checkRulesForMedia(Media media) {
        User currentUser = getCurrentUser();
        WatchList list = listService.findListById(media.getListId());
        return list.getUserId().equals(currentUser.getId());
    }

    @Override
    public boolean checkRulesForList(WatchList watchList) {
        User currentUser = getCurrentUser();
        return watchList.getUserId().equals(currentUser.getId());
    }

    @Override
    public void deleteSelf() {
        User currentUser = getCurrentUser();
        List<WatchList> listsByUserId = listService.findListsByUserId(currentUser.getId());
        listsByUserId.forEach(list -> listService.deleteList(list.getId(), true));
        jdbcTemplate.update("delete from users where id=?", currentUser.getId());
    }

}
