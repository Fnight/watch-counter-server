package ru.fnight.watchcounter.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.fnight.watchcounter.domain.Media;
import ru.fnight.watchcounter.domain.MovieInformation;
import ru.fnight.watchcounter.domain.SerialInformation;
import ru.fnight.watchcounter.service.ExtDatabaseService;
import ru.fnight.watchcounter.service.MediaService;
import ru.fnight.watchcounter.web.rest.dto.QueryResultDto;

@Service
public class ExtDatabaseServiceImpl implements ExtDatabaseService {

    Logger logger = LogManager.getLogger();

    @Value("${extDatabaseToken}")
    private String extDatabaseToken;

    public static String API_URL = "https://api.themoviedb.org/3";

    @Autowired
    private MediaService mediaService;

    @Override
    public List<QueryResultDto> queryFromExtDatabase(String query) throws IOException {
        String url = API_URL + "/search/multi?api_key=" + extDatabaseToken +
                "&language=ru-RU&query=" + URLEncoder.encode(query, "UTF-8");

        URL objUrl = new URL(url);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JsonNode rootNode = mapper.readTree(objUrl);

        return mapper.readValue(rootNode.get("results").toString(), new TypeReference<List<QueryResultDto>>(){});
    }

    private Object getMediaInformation(String type, Long extId) throws IOException {
        String url = API_URL + "/" + type + "/" + extId +"?api_key=" + extDatabaseToken +
                "&language=ru-RU";

        URL objUrl = new URL(url);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JsonNode rootNode;
        try {
            rootNode = mapper.readTree(objUrl);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(type + " with id '" + extId + "' not found");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("error wile read json from url");
        }
        if (type.equals(Media.MOVIE_TYPE)) {
            return mapper.readValue(rootNode.toString(), new TypeReference<MovieInformation>(){});
        } else {
            SerialInformation serial = mapper.readValue(rootNode.toString(),
                    new TypeReference<SerialInformation>(){});
            serial.setEpisodesCount(mapper.readTree(objUrl).get("seasons")
                    .findValuesAsText("episode_count").stream()
                    .map(Integer::parseInt).collect(Collectors.toList()));
            return serial;
        }
    }

    @Override
    @Cacheable("movie")
    public MovieInformation findMovieInformationById(Long extId) throws IOException {
        return (MovieInformation) getMediaInformation(Media.MOVIE_TYPE, extId);
    }

    @Override
    @Cacheable("serial")
    public SerialInformation findSerialInformationById(Long extId) throws IOException {
        return (SerialInformation) getMediaInformation(Media.SERIAL_TYPE, extId);
    }

    @Override
    public void updateMovieCache() {
        mediaService.findAllUniqueMovieExtId().forEach(id -> {
            try {
                long startTime = System.currentTimeMillis();
                findMovieInformationById(id);
                logger.info("CACHE update movie with id = " + id + " in " + (System.currentTimeMillis() - startTime) + " mls");
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (IOException | InterruptedException e) {
                logger.error(e);
            }
        });
    }

    @Override
    public void updateSerialCache() {
        mediaService.findAllUniqueSerialExtId().forEach(id -> {
            try {
                long startTime = System.currentTimeMillis();
                findSerialInformationById(id);
                logger.info("CACHE update serial with id = " + id + " in " + (System.currentTimeMillis() - startTime) + " mls");
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (IOException | InterruptedException e) {
                logger.error(e);
            }
        });
    }
}
