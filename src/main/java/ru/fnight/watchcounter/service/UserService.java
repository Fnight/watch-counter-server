package ru.fnight.watchcounter.service;

import java.sql.Timestamp;
import ru.fnight.watchcounter.domain.Media;
import ru.fnight.watchcounter.domain.User;
import ru.fnight.watchcounter.domain.WatchList;
import ru.fnight.watchcounter.web.rest.dto.UserRegisterDto;

public interface UserService {

    User register(UserRegisterDto userRegisterDto, String ip);

    Timestamp lastRegistration(String ip);

    User getUserByName(String name);

    User getCurrentUser();

    User getUserById(Long id);

    User createNewUser(String name, String rawPassword, String email);

    boolean checkRulesForMedia(Media media);

    boolean checkRulesForList(WatchList watchList);

    void deleteSelf();
}
