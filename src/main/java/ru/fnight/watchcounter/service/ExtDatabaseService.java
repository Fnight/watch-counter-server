package ru.fnight.watchcounter.service;

import java.io.IOException;
import java.util.List;
import ru.fnight.watchcounter.domain.MovieInformation;
import ru.fnight.watchcounter.domain.SerialInformation;
import ru.fnight.watchcounter.web.rest.dto.QueryResultDto;

public interface ExtDatabaseService {
    List<QueryResultDto> queryFromExtDatabase(String query) throws IOException;

    MovieInformation findMovieInformationById(Long extId) throws IOException;

    SerialInformation findSerialInformationById(Long extId) throws IOException;

    void updateMovieCache();

    void updateSerialCache();
}
