package ru.fnight.watchcounter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    @Autowired
    ExtDatabaseService extDatabaseService;

    @Scheduled(fixedDelay = 86400000) // 1 day in milliseconds
    public void updateCache() {
        extDatabaseService.updateMovieCache();
        extDatabaseService.updateSerialCache();
    }

}
