package ru.fnight.watchcounter.service;

import java.util.List;
import ru.fnight.watchcounter.domain.Media;
import ru.fnight.watchcounter.domain.Movie;
import ru.fnight.watchcounter.domain.Serial;

public interface MediaService {
    Media addNewMedia(String mediaType, Long extId, Long listId);

    Media findMediaById(String mediaType, Long id);

    Serial findSerialById(Long id);

    Movie findMovieById(Long id);

    List<Media> findMediasByListId(Long listId);

    void deleteMediaById(Long id, String mediaType);

    Movie updateMovie(Movie movie);

    Serial updateSerial(Serial serial);

    List<Long> findAllUniqueMovieExtId();

    List<Long> findAllUniqueSerialExtId();
}
