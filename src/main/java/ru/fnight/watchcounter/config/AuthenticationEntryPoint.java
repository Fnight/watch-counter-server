package ru.fnight.watchcounter.config;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import ru.fnight.watchcounter.web.rest.dto.ErrorDto;

@Component
public class AuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    @Override
    public void commence
            (HttpServletRequest request, HttpServletResponse response, AuthenticationException authEx)
            throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        PrintWriter writer = response.getWriter();
        ErrorDto errorDto = new ErrorDto(authEx.getMessage(), HttpStatus.UNAUTHORIZED, "you must be logged in");
        writer.println(errorDto.toJsonString());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setRealmName("Watch Counter");
        super.afterPropertiesSet();
    }
}
