package ru.fnight.watchcounter.config;

import java.nio.charset.StandardCharsets;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import ru.fnight.watchcounter.service.ExtDatabaseService;

@Configuration
public class InMemoryConfig {

    Logger logger = LogManager.getLogger();

    private final String INITIAL_DATA = "/start.sql";

    @Autowired
    private DataSource dataSource;

    @Autowired
    private ExtDatabaseService extDatabaseService;

    @PostConstruct
    public void loadIfInMemory() throws Exception {

        ScriptUtils.executeSqlScript(dataSource.getConnection(),
                new EncodedResource(new ClassPathResource(INITIAL_DATA), StandardCharsets.UTF_8));

        extDatabaseService.updateMovieCache();
        extDatabaseService.updateSerialCache();

    }
}
