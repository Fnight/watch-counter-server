package ru.fnight.watchcounter.web.rest;

import java.io.IOException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.fnight.watchcounter.domain.MovieInformation;
import ru.fnight.watchcounter.domain.SerialInformation;
import ru.fnight.watchcounter.service.ExtDatabaseService;
import ru.fnight.watchcounter.web.rest.dto.QueryResultDto;

@RestController
@RequestMapping("/database")
public class ExtDatabaseResource {

    @Autowired
    ExtDatabaseService extDatabaseService;

    private Logger logger = LogManager.getLogger(this.getClass());

    @RequestMapping(value = "/query", method = RequestMethod.GET)
    @ResponseBody
    public List<QueryResultDto> query(@RequestParam String query) throws IOException {
        return extDatabaseService.queryFromExtDatabase(query);
    }

    @RequestMapping(value = "/movie/{id}", method = RequestMethod.GET)
    @ResponseBody
    public MovieInformation getMovieInformation(@PathVariable Long id) throws IOException {
        long startTime = System.currentTimeMillis();
        MovieInformation information = extDatabaseService.findMovieInformationById(id);
        logger.info("GET movie information at " + (System.currentTimeMillis() - startTime));
        return information;
    }

    @RequestMapping(value = "/serial/{id}", method = RequestMethod.GET)
    @ResponseBody
    public SerialInformation getSerialInformation(@PathVariable Long id) throws IOException {
        long startTime = System.currentTimeMillis();
        SerialInformation information = extDatabaseService.findSerialInformationById(id);
        logger.info("GET serial information at " + (System.currentTimeMillis() - startTime));
        return information;
    }
}
