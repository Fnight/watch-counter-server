package ru.fnight.watchcounter.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.fnight.watchcounter.domain.Media;
import ru.fnight.watchcounter.domain.Movie;
import ru.fnight.watchcounter.domain.Serial;
import ru.fnight.watchcounter.service.MediaService;

@RestController
@RequestMapping("/media")
public class MediaResource {

    @Autowired
    MediaService mediaService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Media findMediaById(@PathVariable Long id, @RequestParam String mediaType) {
        return mediaService.findMediaById(mediaType, id);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteMediaById(@PathVariable Long id, @RequestParam String mediaType) {
        mediaService.deleteMediaById(id, mediaType);
    }

    @RequestMapping(value = "/serial", method = RequestMethod.PUT)
    @ResponseBody
    public Serial updateSerial(@RequestBody Serial serial) {
        return mediaService.updateSerial(serial);
    }

    @RequestMapping(value = "/movie", method = RequestMethod.PUT)
    @ResponseBody
    public Movie updateMovie(@RequestBody Movie movie) {
        return mediaService.updateMovie(movie);
    }
}
