package ru.fnight.watchcounter.web.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.fnight.watchcounter.domain.Media;
import ru.fnight.watchcounter.domain.User;
import ru.fnight.watchcounter.domain.WatchList;
import ru.fnight.watchcounter.service.ListService;
import ru.fnight.watchcounter.service.MediaService;
import ru.fnight.watchcounter.service.UserService;
import ru.fnight.watchcounter.web.rest.dto.WatchListDto;

@RestController
@RequestMapping("/list")
public class WatchListResource {

    @Autowired
    UserService userService;

    @Autowired
    ListService listService;

    @Autowired
    MediaService mediaService;

    @RequestMapping(value =  "", method = RequestMethod.POST)
    @ResponseBody
    public WatchList newList(@RequestBody WatchListDto dto) {
        User curUser = userService.getCurrentUser();
        return listService.createList(dto.getName(), dto.getDescription(), curUser.getId(), true);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public WatchList getList(@PathVariable Long id) {
        return listService.findListById(id);
    }

    @RequestMapping(value = "/{listId}/media", method = RequestMethod.POST)
    @ResponseBody
    public Media addNewMedia(@PathVariable("listId") Long listId, @RequestParam String mediaType,
                             @RequestParam Long extId) {
        return mediaService.addNewMedia(mediaType, extId, listId);
    }

    @RequestMapping(value = "/my", method = RequestMethod.GET)
    @ResponseBody
    public List<WatchList> getListsForCurrentUser() {
        return listService.findListsByUserId(userService.getCurrentUser().getId());
    }

    @RequestMapping(value = "/{listId}/medias", method = RequestMethod.GET)
    @ResponseBody
    public List<Media> findMediasByListId(@PathVariable Long listId) {
        return mediaService.findMediasByListId(listId);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public WatchList updateList(@RequestBody WatchList watchList) {
        return listService.updateList(watchList);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteListById(@PathVariable Long id) {
        listService.deleteList(id, false);
    }
}
