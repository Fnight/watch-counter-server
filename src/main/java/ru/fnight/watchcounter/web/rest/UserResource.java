package ru.fnight.watchcounter.web.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.fnight.watchcounter.domain.User;
import ru.fnight.watchcounter.service.UserService;
import ru.fnight.watchcounter.web.rest.dto.UserRegisterDto;

@RestController
@RequestMapping("/user")
public class UserResource {

    @Autowired
    UserService userService;

    private static Logger logger = LogManager.getLogger();

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public User register(@RequestBody UserRegisterDto userRegisterDto, HttpServletRequest request) {
        logger.info("REST POST register new user");
        return userService.register(userRegisterDto, request.getRemoteAddr());
    }

    @RequestMapping(value = "/current", method = RequestMethod.GET)
    @ResponseBody
    public User login() {
        return userService.getCurrentUser();
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
    }

    @RequestMapping(value = "/me", method = RequestMethod.DELETE)
    public void deleteSelf(HttpServletRequest request, HttpServletResponse response) {
        userService.deleteSelf();
        logout(request, response);
    }

    @RequestMapping(value = "/me", method = RequestMethod.GET)
    @ResponseBody
    public User getCurrentUser() {
        return userService.getCurrentUser();
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    @ResponseBody
    public boolean checkAuthorization() {
        return true;
    }
}
