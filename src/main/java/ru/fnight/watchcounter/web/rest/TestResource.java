package ru.fnight.watchcounter.web.rest;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.fnight.watchcounter.service.TestService;

@RestController
@RequestMapping("/test")
public class TestResource {

    @Autowired
    private TestService testService;

    private static Logger logger = LogManager.getLogger();

    @RequestMapping(value = "/getting", method = RequestMethod.GET)
    public String getting() {
        logger.info("REST GET getting");
        return "test";
    }

    @RequestMapping(value = "/secret", method = RequestMethod.GET)
    public String secretGetting() {
        return "\nSECREEEET";
    }
}
