package ru.fnight.watchcounter.web.rest.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.springframework.http.HttpStatus;

public class ErrorDto {
    private String message;
    private Date timestamp;
    private HttpStatus status;
    private int code;
    private List<String> errors;

    public ErrorDto(String message, HttpStatus status, List<String> errors) {
        this.message = message;
        this.status = status;
        this.code = status.value();
        this.timestamp = new Date(System.currentTimeMillis());
        this.errors = errors;
    }

    public ErrorDto(String message, HttpStatus status, String error) {
        this.status = status;
        this.message = message;
        this.code = status.value();
        this.timestamp = new Date(System.currentTimeMillis());
        errors = Collections.singletonList(error);
    }

    public ErrorDto(String message, HttpStatus status) {
        this.status = status;
        this.message = message;
        this.code = status.value();
        this.timestamp = new Date(System.currentTimeMillis());
        errors = Collections.singletonList(message);
    }

    public String toJsonString() {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode objectNode = mapper.createObjectNode();
        objectNode.put("message", this.message);
        objectNode.put("timestamp", timestamp.toString());
        objectNode.put("code", code);
        objectNode.put("status", status.getReasonPhrase());
        ArrayNode errorsNode = mapper.createArrayNode();
        for (String error : errors) {
            errorsNode.add(error);
        }
        objectNode.putPOJO("errors", errorsNode);
        return objectNode.toString();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
