create table if not exists users (
  id serial not null primary key,
  username varchar(45) not null unique,
  password VARCHAR(100) not null,
  email varchar(50),
  role varchar(40) not null default 'ROLE_USER');

create table if not exists list(
  id serial not null primary key,
  user_id bigint references users(id) not null,
  name varchar(40),
  description varchar(200),
  canBeDeleted boolean default true
);

create table if not exists serial (
  id serial not null primary key,
  list_id bigint not null references list(id),
  ext_id bigint not null,
  watched_series integer default 0,
  rating integer default null
);

create table if not exists movie (
  id serial not null primary key,
  list_id bigint not null references list(id),
  ext_id bigint not null,
  watched boolean not null default false,
  rating integer default null
);

create table if not exists registrations(
  ip varchar(20),
  timestamp timestamp
);