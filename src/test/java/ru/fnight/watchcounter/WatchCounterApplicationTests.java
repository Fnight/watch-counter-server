package ru.fnight.watchcounter;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = WatchCounterApplicationTests.class)
public class WatchCounterApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void bcyptTest() {
        String hashpw = BCrypt.hashpw("123", BCrypt.gensalt());
        Assert.assertTrue(BCrypt.checkpw("123", hashpw));
    }

}
